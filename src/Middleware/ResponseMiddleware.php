<?php

namespace Moapi\Middleware;

use Closure;
use Illuminate\Http\JsonResponse;

class ResponseMiddleware
{


    public function handle($request, Closure $next)
    {
        $response = $next($request);
        $data = $response->original;
        //  如果是系统异常抛出
        if ($response->exception) {
            return $data;
        } //  如果是自定义异常抛出
        else if (isset($data->code) && isset($data->msg)) {
            return response()->json(['code' => $data->code, 'data' => $data->data, 'msg' => $data->msg], $data->code)->setEncodingOptions(JSON_UNESCAPED_UNICODE);
        } //  如果是正常返回
        else {
            return response()->json(['code' => 200, 'data' => $data, 'msg' => ''])->setEncodingOptions(JSON_UNESCAPED_UNICODE);

        }
    }
}
