<?php

namespace Moapi;

class Routes
{
    public function getHostUrl()
    {
        return $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'];
    }

    public function getRoutes()
    {
        $namespace = $this->getProjectNamespace();
        $service = explode('.', $this->getService());
        $action = array_pop($service);
        return ($namespace ? $namespace . '\\' : '') . implode('\\', $service) . 'Controller@' . $action;
    }

    public function getProjectNamespace()
    {
        $config = Config::getProjectList();
        $p = $this->input('p');
        return $config['/' . $p] ?? $config['/'];
    }

    public function routesCheck($namespace)
    {
        try {
            $namespace = explode('@', $namespace);
            $class = new \ReflectionClass($namespace[0]);
            $method = $class->getMethod($namespace[1]);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }


    public function getProjectUri()
    {
        $p = $this->input('p');
        return '/' . $p;
    }

    public function getMethod()
    {
        $service = explode('.', $this->getService());
        return strtolower(array_pop($service));
    }

    public function getService()
    {
        return $this->input('service', $this->input('s'));
    }

    public function input($key, $default = '')
    {
        $data = [];
        $postRaw = file_get_contents('php://input');
        if (!empty($postRaw)) {
            $postRawArr = json_decode($postRaw, TRUE);
            if (!empty($postRawArr) && is_array($postRawArr)) {
                $data = $postRawArr;
            }
        }
        $data = array_merge($_REQUEST, $data);
        $data = array_merge($_POST, $data);

        return $data[$key] ?? $default;
    }
}
