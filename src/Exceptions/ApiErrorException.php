<?php

namespace Moapi\Exceptions;

use Exception;
use Illuminate\Http\Request;

class ApiErrorException extends Exception
{
    protected $errorData;

    public function __construct($data)
    {
        $this->errorData = $data;
    }

    public function render(Request $request)
    {
        return response()->json(['code' => $this->errorData['code'], 'data' => $this->errorData['data'], 'msg' => $this->errorData['msg']],400)->setEncodingOptions(JSON_UNESCAPED_UNICODE);
    }
}
